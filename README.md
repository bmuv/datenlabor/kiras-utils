# kiras-utils

## To run locally:

Prepare some testfile `test.json` for uploading.

Then,
```
$env:NO_PROXY =''
python python .\upload_download_script.py .\test.json <Download-Ordner> <SIB-Box-Testordner>
```
In addition, create a .env-file similar to `.env-example` with your sibbox-credentials and your proxy information.

## To run on the server:
- adjust the inputs in Dockerfile (paths to upload / download)
- make sure housekeeping on the volume is congruent with those paths / files are where you think they should be via ssh
- make sure your sibbox has the files in the correct dir as well

## Expected file structure
- data-dev
    - kiras
        - bmuv-gvpl
            - <our gvpl>.json
            - archive
        - share-download
            - <downloaded gvpl>.json
- SIBbox
    - KIRAS
        - <misc gvpl>.json


## Funding notice
![funding notice](assets/DE_Finanziert_von_der_Europäischen_Union_RG_POS.png)