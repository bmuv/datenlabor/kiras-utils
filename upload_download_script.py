import sys
import os
import nc_py_api
import json
from dotenv import load_dotenv


# function to test different encodings
def has_encoding(content_, encoding_):
    try:
        dict_ = json.loads(content_.decode(encoding_))  # noqa
    except UnicodeDecodeError:
        return False
    return True


load_dotenv()

# get env vars
nextcloud_url = os.environ.get("NC_URL")
nc_auth_user = os.environ.get("NC_USER")
nc_auth_pass = os.environ.get("NC_PASSWORD")

# get inputs
upload_file = sys.argv[1]  # file to upload
download_dir = sys.argv[2]  # directory to save downloads
kiras_dir = sys.argv[3]  # directory on sibbox

# connect to sib-box
print("connecting to sibbox")
sib_box = nc_py_api.Nextcloud(
    nextcloud_url=nextcloud_url,
    nc_auth_user=nc_auth_user,
    nc_auth_pass=nc_auth_pass,
)

# upload
print("uploading...")
with open(upload_file, "r") as f:
    upload_target = kiras_dir + "/BMUV_tasks.json"
    content = f.read()
    sib_box.files.upload(upload_target, content)
    print("uploaded to " + kiras_dir)

# download
print("downloading...")
for node in sib_box.files.listdir(kiras_dir):
    if node.user_path.split(".")[-1].lower() == "json":
        print("\t" + node.user_path)
        content = sib_box.files.download(node.user_path)
        # try different encodings
        try:
            if has_encoding(content, "utf-8-sig"):
                dict_ = json.loads(content.decode("utf-8-sig"))
            elif has_encoding(content, "utf-8"):
                dict_ = json.loads(content.decode("utf-8"))
            else:
                dict_ = json.loads(content.decode("utf-8"))
        except Exception as e:
            print(
                f"Error converting {node.user_path} to GVPL: {e}. Skipping this file."
            )
        # write back to file
        with open(download_dir + "/" + node.name, "w") as f:
            json.dump(dict_, f)
print("downloaded to " + download_dir)
