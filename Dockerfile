FROM python:3.12.2-alpine

COPY requirements.txt .
COPY .netrc root/.netrc 
# ^^^ nexus auth 

ENV PIP_TRUSTED_HOST repo.office.dir
ENV PIP_INDEX http://repo.office.dir/repository/pypi-proxy/pypi
ENV PIP_INDEX_URL http://repo.office.dir/repository/pypi-proxy/simple

COPY upload_download_script.py .
RUN pip install -r requirements.txt
RUN pip freeze > lock.txt

ENTRYPOINT ["python",  "upload_download_script.py", "/data/kiras/bmuv-gvpl/BMUV_tasks.json", "/data/kiras/share-download", "KIRAS"]



